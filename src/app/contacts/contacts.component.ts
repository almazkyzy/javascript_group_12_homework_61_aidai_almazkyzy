import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent {
  isModalOpen = false;
  isTYModalOpen = false;

  openLeaveNumberModal() {
    this.isModalOpen = true;
  }
  closeLeaveNumberModal() {
    this.isModalOpen = false;
  }
  thankYou() {
    this.isTYModalOpen = true;
  }
  closeTYModal() {
    this.isTYModalOpen = false;
  }
}
