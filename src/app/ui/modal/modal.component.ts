import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() isOpen = false;
  @Input() title: string = 'Modal';
  @Output() close = new EventEmitter<void>();
  onClose() {
    this.close.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
