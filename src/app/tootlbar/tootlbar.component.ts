import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-tootlbar',
  templateUrl: './tootlbar.component.html',
  styleUrls: ['./tootlbar.component.css']
})
export class TootlbarComponent {
  @Output() navigate = new EventEmitter<string>();
  onNavClick(where: string) {
    this.navigate.emit(where);
  }
  constructor() { }
}
